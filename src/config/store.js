import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import login from '../reducers/LoginReducer'
import passwordList from '../reducers/PasswordListReducer'

const rootReducer = combineReducers({
  login: login,
  passwordList: passwordList
})

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['login', 'passwordList']
}

const persistedReducer = persistReducer(persistConfig, rootReducer);

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  persistedReducer,
  composeEnhancers(applyMiddleware())
);

const persistor = persistStore(store);

export { store, persistor };