import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import Home from '../components/Home';
import Header from '../components/Header';
import PasswordsPage from '../components/PasswordsPage';
import Login from '../components/Login';
import Unauthorized from '../components/Unauthorized';
import NewPassword from '../components/NewPassword';
import ManagePasswords from '../components/ManagePasswords';
import Logout from '../components/Logout';

class Routes extends React.Component {
  render() {
    return (
      <div className="ui container">
        <BrowserRouter>
          <Header/>
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/login" render={() => !this.props.isLogged ? <Login/> : <Redirect to={{pathname:"/password/list"}}/>}/>
            <Route path="/password/list" render={() => <PasswordsPage /> } />
            <Route path="/password/new" component={NewPassword} />
            <Route path="/password/manage" render={() => this.props.isLogged ? <ManagePasswords/> : <Unauthorized/>}/>
            <Route path="/logout" component={Logout} />
          </Switch>
        </BrowserRouter>
      </div>
    )
  }
}

const mapStateToProps = ( state ) => {
  return { isLogged: state.login.isLogged };
}

export default connect( mapStateToProps )( Routes );