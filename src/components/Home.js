import React from 'react';
import { Link } from 'react-router-dom';

class Home extends React.Component {
  render() {
    return (
      <div className="ui raised very padded text container segment">
        <div className="ui form sucess">
          <div className="field">
            <div>
              <label>Clique para iniciar o sistema</label>
            </div>
            <div>
              <Link to="/password/list" className="item">
                <input type="button" className="ui button" value="INICIAR"/>
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;