import React from 'react';
import { connect } from 'react-redux';

import PasswordListHeader from './PasswordListHeader';
import RegularPasswords from './RegularPasswords';
import SpecialPasswords from './SpecialPasswords';
import { getAllRegularPasswords, getAllSpecialPasswords } from '../actions'
import ParipassuApi from '../api/ParipassutApi';
import { NORMAL, PREFERENCIAL } from '../passwordUtils/passwordTypes';

class PasswordPage extends React.Component {
  state = {
    apiError: false
  }

  componentDidMount() {
    this.getRegularPasswords();
    this.getSpecialPasswords();
  }

  getRegularPasswords = async props => {
    try {
      const response = await ParipassuApi.get( `/password?type=${NORMAL}` );
      this.props.getAllRegularPasswords( response.data );
    } catch ( e ) {
      this.setState( { apiError: true } );
    }
  }

  getSpecialPasswords = async props => {
    try {
      const response = await ParipassuApi.get( `/password?type=${PREFERENCIAL}` );
      this.props.getAllSpecialPasswords( response.data );
    } catch ( e ) {
      this.setState( { apiError: true } );
    }
  }

  render() {
    return (
      <div>
        <PasswordListHeader />
        <div className="ui center aligned two column grid">
          <div className="column">
            <RegularPasswords regularPasswords={this.props.regularPasswords}/>
          </div>
          <div className="column">
            <SpecialPasswords specialPasswords={this.props.specialPasswords}/>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = ( { passwordList } ) => {
  return { regularPasswords: passwordList.regularPasswords, specialPasswords: passwordList.specialPasswords };
};

export default connect( mapStateToProps, { getAllRegularPasswords, getAllSpecialPasswords } )( PasswordPage );