import React from 'react';
import { connect } from 'react-redux';

import { signOut } from '../actions'

class LogOut extends React.Component {
  state = {
    errorMessage: 'Nenhum usuário logado'
  }

  onFormSubmit = event => {
    event.preventDefault();
    if ( this.props.isLogged ) {
      this.logOutClient( this.props );
    } else {
      this.setState( { errorMessage: 'Usuário já deslogado' } );
    }
  }

  logOutClient = async props => {
    this.props.signOut(false, null);
  }
  
  render() {
    return (
      <div className="ui raised very padded text container segment">
        <div className={this.props.isLogged ? "ui warning message hidden" : "ui warning message visible"}>
          <div className="header">
            {this.state.errorMessage}
          </div>
        </div>
        <div className="ui form sucess">
          <form onSubmit={this.onFormSubmit} className="ui form sucess" id="formulario">
            <h4 className="ui dividing header">LOGOUT</h4>
            <div className="field">
              <h4>Deseja realmente sair?</h4>
            </div>
            <div className="field">
              <input type="submit" className="ui button" value="Sair"/>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ( state ) => {
  return { isLogged: state.login.isLogged };
};

export default connect( mapStateToProps, { signOut } )( LogOut );