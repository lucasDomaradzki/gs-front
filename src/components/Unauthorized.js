import React from 'react';

const Unauthorized = () => {
  return (
    <div className="ui container">
      <div className="field">
          <div className="ui raised very padded text container segment">
            Realize o login para obter acesso
          </div>
      </div>
    </div>
  );
}

export default Unauthorized;