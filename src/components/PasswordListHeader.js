import React from 'react';

const PasswordListHeader = () => {
  return (
    <div>
      <div className="ui center aligned two column grid">
        <div className="column">
          <h3>SENHAS NORMAIS:</h3>
        </div>
        <div className="column">
          <h3>SENHAS PREFERENCIAIS:</h3>
        </div>
      </div>
    </div>
  )
}

export default PasswordListHeader;