import React from 'react';

const PasswordItem = ( { password } ) => {
  return (
    <div className="ui segment">
      <h1 key={password}>{password}</h1>
    </div>
  )
};

export default PasswordItem;