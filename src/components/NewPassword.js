import React from 'react';
import InputMask from 'react-input-mask';

import ParipassuApi from '../api/ParipassutApi';
import { NORMAL, PREFERENCIAL } from '../passwordUtils/passwordTypes';

class NewPassword extends React.Component {
  state = {
    apiError: false,
    currentRegularPassword: 'Carregando...',
    currentSpecialPassword: 'Carregando...'
  }

  componentDidMount() {
    this.loadLatestNormalPassword();
    this.loadLatestSpecialPassword();
  }

  loadLatestNormalPassword = async props => {
    try {
      const response = await ParipassuApi.get( `/password/manage?type=${NORMAL}` );
      this.setState( { currentRegularPassword: response.data.value } );
    } catch ( error ) {
      this.setState({apiError: true});
    }
  }

  loadLatestSpecialPassword = async props => {
    try {
      const response = await ParipassuApi.get( `/password/manage?type=${PREFERENCIAL}` );
      this.setState( { currentSpecialPassword: response.data.value } );
    } catch ( error ) {
      this.setState( { apiError: true } );
    }
  }

  newRegularPassword = async props => {
    try {
      const response = await ParipassuApi.post( `/password?type=${NORMAL}` );
      this.setState( { currentRegularPassword: response.data.value } );
    } catch ( error ) {
      this.setState( { apiError: true } );
    }
  }

  newSpecialPassword = async props => {
    try {
      const response = await ParipassuApi.post( `/password?type=${PREFERENCIAL}` );
      this.setState( { currentSpecialPassword: response.data.value } );
    } catch ( error ) {
      this.setState( { apiError: true } );
    }
  }
  
  render() {
    return (
      <div className="ui container">
        <div className="field">
          <div className="three field">
            <div className={this.state.apiError ? "ui raised very padded text container warning message visible" : "ui raised very padded text container warning message hidden"}>
              <div className="header">
                Erro ao carregar últimas senhas
              </div>
            </div>
            <div className="ui raised very padded text container segment">
              <InputMask type="button" className="ui button" value="Nova Senha Normal" onClick={this.newRegularPassword} />
              <div className="ui huge label">
                Senha atual: {this.state.currentRegularPassword}
              </div>
            </div>
            <div className="ui raised very padded text container segment">
              <InputMask type="button" className="ui button" value="Nova Senha Preferencial" onClick={this.newSpecialPassword} />
              <div className="ui huge label">
              Senha atual: {this.state.currentSpecialPassword}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  };
}

export default NewPassword;