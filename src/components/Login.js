import React from 'react';
import InputMask from 'react-input-mask';
import { connect } from 'react-redux';

import ParipassuApi from '../api/ParipassutApi';
import { signIn, signOut } from '../actions'

class Login extends React.Component {
  state = {
    email: '',
    password: '',
    apiError: false
  }
  
  logInClient = async props => {
    this.setState( { apiError: false } );

    try {
      const response = await ParipassuApi.patch("/client/login", this.state);
      this.props.signIn(response.data);
    } catch ( e ) {
      this.setState({apiError: true});
    }
  }

  handleChange = event => {
    let value = event.target.value;
    this.setState( { [event.target.name]: value } );
  }

  onFormSubmit = event => {
    event.preventDefault();
    if ( !this.isLogged ) {
      this.logInClient( this.props );
    }
  }

  render() {
    return (
      <div className="ui raised very padded text container segment">
        <div className={this.state.apiError ? "ui warning message visible" : "ui warning message hidden"}>
          <div className="header">
            Email ou senha inválidos
          </div>
          Tente novamente
        </div>
        <div className="ui form sucess">
          <form onSubmit={this.onFormSubmit} className="ui form sucess" id="formulario">
            <h4 className="ui dividing header">LOGIN</h4>
            <div className="required field">
              <label>E-mail</label>
              <InputMask type="text" name="email" value={this.state.nome} onChange={this.handleChange} placeholder="email@email.com"/>
            </div>
            <div className="required field">
              <label>Senha</label>
              <InputMask type="password" name="password" onChange={this.handleChange} placeholder="*****"/>
            </div>
            <div className="field">
              <input type="submit" className="ui button" value="Login"/>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ( state ) => {
  return { isLogged: state.isLogged };
};

export default connect( mapStateToProps, { signIn, signOut } )( Login );