import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

class Header extends React.Component {
  render() {
    return (
      <div>
        <Link to="/" className="item"><h4>GERENCIADOR DE SENHAS</h4></Link>
        <div className="ui secondary pointing menu">
          <div className="left menu">
            <div>
              <Link to="/password/list" className="item">Acompanhar Senhas</Link>
            </div>
          </div>
          <div>
            <div>
              <Link to="/password/new" className="item">Solicitar nova senha</Link>
            </div>
          </div>
          <div className="right menu">
            <div>
              <Link to="/password/manage" className="item">Gerenciar senhas</Link>
            </div>
            <div>
              <Link to="/logout" className="item">Sair</Link>
            </div>
            <div>
              <Link to="/login" className="item">{this.props.isLogged ? `${this.props.userName}` : "Entrar"}</Link>
            </div>
          </div>
        </div>
      </div>
    )
  }
};

const mapStateToProps = ( state ) => {
  return { isLogged: state.login.isLogged, userName: state.login.userName }
}

export default connect( mapStateToProps )( Header );