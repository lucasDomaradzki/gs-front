import React from 'react';

import PasswordItem from './PasswordItem';
import { P } from '../passwordUtils/passwordPrefixes';

const SpecialPasswords = ( { specialPasswords } ) => {
  if ( !specialPasswords ) {
    return <PasswordItem key={"NA"} password={"Nenhuma senha"}/>
  }

  const renderSpecials = specialPasswords.map( ( specialPassword ) =>{
    const newPassword = `${P}${specialPassword}`;
    return <PasswordItem key={specialPassword} password={newPassword}/>
  })

  return renderSpecials;
}

export default SpecialPasswords;