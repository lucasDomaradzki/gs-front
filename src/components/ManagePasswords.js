import React from 'react';
import { connect } from 'react-redux';

import { getAllRegularPasswords, getAllSpecialPasswords } from '../actions'
import ParipassuApi from '../api/ParipassutApi';

class ManagePassword extends React.Component {
  state = {
    apiStatus: false,
    message: '',
    passwordNumber: null,
    displayPassword: 'N.A.',
    passwordType: '',
    passwordTypePrefix: ''
  }

  componentDidMount() {
    this.getCurrentPassword( this.props );
  }

  onCallPasswordClick = () => {
    this.callNextPassword( this.state );
  }

  onResetClick = () => {
    this.resetAllPasswords( this.props );
  }

  resetAllPasswords = async props => {
    this.setState( { apiStatus: false, message: '' } );

    try {
      await ParipassuApi.put( `password/reset` );

      this.props.getAllRegularPasswords( [] );
      this.props.getAllSpecialPasswords( [] );

      this.setState( { apiStatus: true, message: 'Senhas zeradas' } );
    } catch ( e ) {
      this.setState( { apiStatus: true, message: 'Erro ao zerar as senhas' } );
    }

    this.getCurrentPassword( this.props );
  }

  callNextPassword = async ( { passwordType, passwordNumber } ) => {
    this.setState( { apiStatus: false, errorMessage: '' } );

    if ( this.state.displayPassword === 'N.A.' ) {
      this.setState( { apiStatus: true, message: 'Não há senha a ser chamada' } );
      return;
    }

    try {
      await ParipassuApi.patch( `/password/${passwordNumber}?type=${passwordType}` );
      this.getCurrentPassword( this.state );
    } catch ( e ) {
      this.setState( { apiStatus: true, message: 'Erro ao chamar próxima senha' } );
    }

    this.getCurrentPassword( this.props );
  }

  getCurrentPassword = async state => {
    try {
      const response = await ParipassuApi.get( '/password/current' );
      this.setState({
        displayPassword: `${response.data.passwordTypePrefix}${response.data.value}`,
        passwordNumber: response.data.value,
        passwordTypePrefix: response.data.passwordTypePrefix,
        passwordType: response.data.passwordType
      });
    } catch ( e ) {
      this.setState( { apiStatus: true, message: 'Erro ao buscar senha a ser chamada' } );
    }

  }

  render() {
    return (
      <div className="ui raised very padded text container segment">
        <div className="ui center aligned two column grid">
          <div className="column">
            <input type="button" className="ui button" value="Chamar próxima senha" onClick={this.onCallPasswordClick}/>
          </div>
          <div className="column">
            <input type="button" className="ui button" value="Zerar senhas" onClick={this.onResetClick}/>
          </div>
          <div className="column">
            <div className="ui segment">
              <h1>{this.state.displayPassword}</h1>
            </div>
          </div>
        </div>
        <div className={this.state.apiStatus ? "ui warning message visible" : "ui warning message hidden"}>
          <div className="header">
            {this.state.message}
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = ( { passwordList } ) => {
  return { regularPasswords: passwordList.regularPasswords, specialPasswords: passwordList.specialPasswords };
}

export default connect( mapStateToProps, { getAllRegularPasswords, getAllSpecialPasswords } )( ManagePassword );