import React from 'react';

import PasswordItem from './PasswordItem';
import { N } from '../passwordUtils/passwordPrefixes';

const PasswordList = ( { regularPasswords } ) => {
  if ( !regularPasswords ) {
    return <PasswordItem key={"NA"} password={"Nenhuma senha"}/>
  }

  const renderRegular = regularPasswords.map( ( regularPassword ) =>{
    const newPassword = `${N}${regularPassword}`;
    return <PasswordItem key={regularPassword} password={newPassword}/>
  })

  return renderRegular;
}

export default PasswordList;