import { GET_NEXT_SPECIAL_PASSWORD, GET_NEXT_REGULAR_PASSWORD } from '../actions/actionTypes'

const INITIAL_STATE = {
  regularPasswords: [],
  specialPasswords: []
}

export default ( state = INITIAL_STATE, action ) => {
  switch ( action.type ) {
    case GET_NEXT_REGULAR_PASSWORD:
      return { ...state, regularPasswords: action.regularPasswords }
    case GET_NEXT_SPECIAL_PASSWORD:
      return { ...state, specialPasswords: action.specialPasswords }
    default:
      return state;
  }
};