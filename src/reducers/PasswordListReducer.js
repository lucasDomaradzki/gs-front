import { ALL_REGULAR_PASSWORDS, ALL_SPECIAL_PASSWORDS } from '../actions/actionTypes'

const INITIAL_STATE = {
  regularPasswords: [],
  specialPasswords: []
}

export default ( state = INITIAL_STATE, action ) => {
  switch ( action.type ) {
    case ALL_REGULAR_PASSWORDS:
      return { ...state, regularPasswords: action.regularPasswords }
    case ALL_SPECIAL_PASSWORDS:
      return { ...state, specialPasswords: action.specialPasswords }
    default:
      return state;
  }
};