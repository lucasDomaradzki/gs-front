import { SIGN_IN, SIGN_OUT } from '../actions/actionTypes';

const INITIAL_STATE = {
  isLogged: false,
  userName: ''
}

export default ( state = INITIAL_STATE, action ) => {
  switch ( action.type ) {
    case SIGN_IN:
      return { ...state, isLogged: true, userName: action.userName }
    case SIGN_OUT:
      return { ...state, isLogged: false, userName: '' }
    default:
      return state;
  }
};