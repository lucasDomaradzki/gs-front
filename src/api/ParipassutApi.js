import Environment from '../environment/Environmet'
import axios from 'axios';

export default axios.create({
  baseURL: Environment.apiBaseUrl
});