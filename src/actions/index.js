import { SIGN_IN, SIGN_OUT, ALL_REGULAR_PASSWORDS, ALL_SPECIAL_PASSWORDS } from './actionTypes'

export const signIn = ( { logged, clientName } ) => {
  return {
    type: SIGN_IN,
    isLogged: logged,
    userName: clientName
  };
}

export const signOut = ( logged, clientName ) => {
  return {
    type: SIGN_OUT,
    isLogged: logged,
    userName: clientName
  };
}

export const getAllRegularPasswords = ( regularPasswords ) => {
  return {
    type: ALL_REGULAR_PASSWORDS,
    regularPasswords: regularPasswords
  };
}

export const getAllSpecialPasswords = ( specialPasswords ) => {
  return {
    type: ALL_SPECIAL_PASSWORDS,
    specialPasswords: specialPasswords
  };
}