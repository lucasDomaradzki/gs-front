## PASSWORD SYSTEM FRONT-END

![Node](https://img.shields.io/badge/node-8.11.4-brightgreen.svg)
![React](https://img.shields.io/badge/react-16.8.6-brightgreen.svg)
![Redux](https://img.shields.io/badge/redux-4.0.1-brightgreen.svg)

### Small business aplication to generate and supervise passwords

### To run the system follow the instructions bellow:
- Node 8.11.4 is required to run this project
- To download dependencies npm should also be installed

Once Node and Npm are installed run the command bellow to download dependencies
- npm i

Then to start project run the next command bellow:
- npm start